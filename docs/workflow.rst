.. _workflow:

Analysis Workflow
=================

track-tools's main goal is to help you scan through your recordings quickly. Your recordings
have to be tiff stacks, a sequence of tiff files, or videos (videos not tested yet,
feedback appreciated).

In general, all command line programs provide information on available parameters and options
with ``<name-of-command> --help``. E.g., for the command ``track-tools-extract-trajectories``
type

.. code:: bash

   $ track-tools-extract-trajectories --help

Extract Trajectories
--------------------

Let's say you have a sequence of tiff files named
``data/frame_001.tif``, ``data/frame_002.tif``, and so on.
You want to extract the trajectories of particles via trackpy, and write
the results to folder ``track_results``.
Use the command

.. code:: bash

   $ track-tools-extract-trajectories "data/frame*.tif" -o track_results

(You need the quotes when you use the ``*`` wildcard, to leave expanding that pattern
to ``track-tools-extract-trajectories``.)
This will start an interactive plot, where you can adjust the tracking parameters. It should
look something like this:

.. image:: _static/images/param_selector.png
   :width: 90%

Adjust diameter and mass (press enter or leave text box for new values to take effect),
and check in a few frames (click on frame slider) that all particles are found correctly.

Before you close the window, check the values for memory, max. dist. (maximum distance
a particle can move between frames) and the minimum number of frames a particle has to
be found in.

When you close the window, you need to confirm that you want to extract trajectories with
the current parameters. Extracted trajectories will be written into a subfolder ``analysis_000``
of the chosen output folder,
``track_results/analysis_000``. The subfolders are always named ``analysis``
with an index suffix. If you execute the extract trajectories command again, it will be
written to ``track_results/analysis_001``.

For the following programs, you usually
**have to provide the path to an analysis folder** as first argument. The trajectories in
that analysis folder will then be used.

Correct for Drift
-----------------

Continue with the trajectories that have been saved to ``track_results/analysis_000``.
In a next step, you want to subtract the drift. Use the command:

.. code::

   $ track-tools-drift-correction track_results/analysis_000

This will spawn an interactive plot window as well. In the plot window, you see the effect of the
drift correction with currently selected smoothing:

.. image:: _static/images/drift_corrector.png

You can adjust the value for the smoothing by cliking into the text box and entering
a different integer value.

When you close the window, the trajectories as seen in the ``corrected`` plot window on the
right-hand side will be saved to the analysis folder.

Read Corrected Trajectories
---------------------------

The two points above, tracking particles and applying drift correction, that's all that
track-tools can do for you. To continue working with the extracted trajectories, you need
to learn how to load them.

This is a short example on how to plot corrected trajectories:

.. code::

   import os
   import matplotlib.pyplot as plt
   import pandas as pd
   import numpy as np
   
   
   analysis_folder = "track_results/analysis_000"
   # If you did the drift correction, there should be the file
   # drift_corrected_trajectories.csv in the chosen folder
   trajectory_file = os.path.join(analysis_folder, 'drift_corrected_trajectories.csv')
   # Otherwise you can just use trajectories.csv, which are the tracked trajectories
   # without correction.
   
   trajectories = pd.read_csv(trajectory_file, index_col=0)
   
   fig, ax = plt.subplots(1, 1)
   for particle in trajectories['particle'].unique():
       # The column 'particle' contains the particle IDs.
       # We use it to extract the trajectories particle
       # by particle.
       traj_p_table = trajectories[trajectories['particle'] == particle]
   
       # Create a numpy array large enough to contain the whole trajectory
       # of the current particle. Gaps are filled with nans (which makes
       # analyses based on the trajectory simpler in many cases)
       traj_p = np.full((traj_p_table.index.max(), 2), np.nan)
       traj_p[traj_p_table.index-1, 0] = traj_p_table['x']
       traj_p[traj_p_table.index-1, 1] = traj_p_table['y']
       ax.plot(traj_p[:, 0], traj_p[:, 1], lw=0.5)
   
   ax.set_aspect('equal')
   ax.invert_yaxis()
   plt.show()
   
