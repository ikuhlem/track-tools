.. track-tools documentation master file, created by
   sphinx-quickstart on Tue Nov  3 14:29:35 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

track-tools
===========

Some scripts and wrappers for trackpy, to speed up the workflow
e.g. for basic tracking and drift correction.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   install
   workflow
   rois
   
