

Using ROIs
**********

You will often be in a situation, where you are interested in particles in certain regions only.
These regions of interest (ROIs) can comfortably be specified with
`ImageJ/Fiji <https://imagej.net/Fiji>`_.

When you have :ref:`selected your regions of interest <ROI How To>` in Fiji, save them either
as *Roi Set* (``.zip`` file), or in separate ``.roi`` files. You can use the tool
``track-tools-extract-trajectories`` with ROIs, in which case all regions outside of ROIs
will be set to black / zero. Specify the ROI file(s) as the last option in the command line
prepended by a ``-r`` or ``--rois``. For example, when you have a stack of tiff images in file
``tiffs/104_brightness_adjusted.tif``, and a set of matching ROIs in folder ``RoiSet 104``, you
you would start the tool from command line like this:

.. code:: bash

   track-tools-extract-trajectories tiffs/104_brightness_adjusted.tif -r RoiSet\ 104/*.roi

On Linux the (forward) slash ``/`` separates folders in your files' paths (i.e., the file
``104_brightness_adjusted.tif`` is in folder ``tiffs`` in the above example). On windows you might have
to use backslash ``\`` to separate folders. The backslash in ``RoiSet\ 104/*.roi`` is to escape
the space in the folder
name, to make sure this sequence is considered as one argument, and not confused as two separate arguments.
You can avoid that if you give folders names without spaces.



.. _ROI How To:

How to Select and Save ROIs with Fiji
=====================================

TODO
