
Install
=======

I recommend using `conda <https://docs.conda.io/en/latest/>`_ for ``track-tools``. (I have made the
experience that trackpy did not work as fast, when I used it outside of conda, and I could
not figure out what made it faster with conda.)

Install these dependencies via conda:

.. code::

   conda update conda
   conda install -c conda-forge trackpy
   conda install -c conda-forge pims
   conda install numba

Then install ``track-tools`` (missing dependencies will be automatically installed) with ``pip``:

.. code::

   pip install track-tools

   
That should be it. Continue with the section on :ref:`Analysis Workflow <workflow>` to learn how
to use ``track-tools``.
